""" Includes the install and version info for the project

This project uses PBR for setup management, including automatic version numbers and change logs.
See: https://docs.openstack.org/pbr/latest/user/using.html
"""

from setuptools import setup

setup(
    setup_requires=['pbr'],
    pbr=True,
)
