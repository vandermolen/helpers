from __future__ import annotations
from pathlib import Path
import sys

from helpers.log import log_exception, log_system_info, init_logging

logger = init_logging(__name__, "python.log")


def main():
    logger.info("Hello world!")


if __name__ == '__main__':
    try:
        logger.info(f"#### Program Started ####")
        log_system_info(logger)

        main()

    except KeyboardInterrupt:
        logger.info("Keyboard interrupt. Program will terminate.")
    except Exception as ex:
        log_exception(logger, ex, "Unexpected exception stopped program:")
    finally:
        logger.info(f"#### Program Ended ####\n")
        if getattr(sys, "frozen", False):
            input("Press Enter to continue...")
