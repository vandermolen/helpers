@echo off
REM # Converts the Python script to a single .exe file (stored in the "dist" folder) for easy distributing
REM # Reference: https://pyinstaller.readthedocs.io/en/stable/usage.html


pyinstaller .\src\main.py ^
--name empty_project ^
--onefile ^
--noconfirm ^
--noupx
