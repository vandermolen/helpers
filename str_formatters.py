"""
Reusable helper functions for formatting text, including timestamps, filenames, etc.

Tommy Vandermolen
23 July 2021
"""

from datetime import datetime
import itertools


def str_filename(text: str):
    """ Converts a string to a legal file name by removing any illegal characters
    Source: https://stackoverflow.com/questions/295135/turn-a-string-into-a-valid-filename

    :param text: the string to convert
    :return: the string with any illegal characters removed
    """
    valid_chars = frozenset('-_() abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    return ''.join(c for c in text.strip() if c in valid_chars)


def str_duration(t: float) -> str:
    """ Formats a time interval (in seconds) as HH:MM:SS """
    hours, remainder = divmod(t, 3600)
    minutes, seconds = divmod(remainder, 60)
    return f"{int(hours):02d}:{int(minutes):02d}:{int(seconds):02d}"


def str_timestamp(incl_date=True, incl_time=True, use_utc=False):
    """ Readable date/time stamp

    Not safe for inclusion in file names. See `str_file_safe_timestamp`

    :param incl_date: whether to include the date portion of the timestamp, defaults to True
    :param incl_time: whether to include the time portion of the timestamp, defaults to True
    :param use_utc: whether to use UTC time (instead of system timezone), defaults to False
    :return: a timestamp string that can be safely inserted in file names
    """

    now = datetime.now() if not use_utc else datetime.utcnow()

    if incl_date and incl_time:
        return now.strftime('%Y-%m-%d %H:%M:%S')
    elif incl_date:
        return now.strftime('%Y-%m-%d')
    else:
        return now.strftime('%H:%M:%S')


def str_file_safe_timestamp(incl_date=True, incl_time=True, use_utc=False):
    """ Readable date/time stamp safe to include in file names

    Contains no spaces or characters illegal in file names

    :param incl_date: whether to include the date portion of the timestamp, defaults to True
    :param incl_time: whether to include the time portion of the timestamp, defaults to True
    :param use_utc: whether to use UTC time (instead of system timezone), defaults to False
    :return: a timestamp string that can be safely inserted in file names
    """

    now = datetime.now() if not use_utc else datetime.utcnow()

    if incl_date and incl_time:
        return now.strftime('%Y-%m-%d_T%H-%M-%S')
    elif incl_date:
        return now.strftime('%Y-%m-%d')
    else:
        return now.strftime('T%H-%M-%S')


def str_bytes(b: bytes, cap=True, group=2):
    """ Formats a bytes object as grouped hex digits, for easier reading

    e.g. b'\xca\xfe\xd0\x0d' -> CAFE D00D
    Note: if you want the string to start with 0x, append it yourself

    :param b: the bytes object to format
    :param cap: if True, hexadecimal letters are capitalized, lowercase otherwise (0x3f vs 0x3F)
    :param group: number of bytes to include in a group, e.g. 0->'CAFEDOOD', 1->'CA FE DO OD', or 2->'CAFE DOOD'
    :return: the formatted string of hex characters
    """
    str_hex = b.hex()
    if cap:
        str_hex = str_hex.upper()
    if group > 0:
        # Group characters and join into string
        n = group * 2
        hex_iters = [iter(str_hex)] * n
        hex_groups = itertools.zip_longest(*hex_iters, fillvalue='')
        str_hex = ' '.join(''.join(group) for group in hex_groups)
    return str_hex
