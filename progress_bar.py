"""
Display a progress bar in the terminal

Tommy Vandermolen
23 July 2021
"""
import sys


def progress_bar(iteration: int, total: int, prefix='', suffix='', decimals=1, bar_length=35):
    """ Call in a loop to create terminal progress bar
    Closely based on: https://gist.github.com/aubricus/f91fb55dc6ba5557fbab06119420dd6a
    :param iteration: current iteration
    :param total: total iterations
    :param prefix: prefix string
    :param suffix: suffix string
    :param decimals: positive number of decimals in percent complete
    :param bar_length: character length of bar
    """
    str_format = "{0:." + str(decimals) + "f}"
    percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '#' * filled_length + '-' * (bar_length - filled_length)

    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),

    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()
