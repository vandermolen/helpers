"""
Read/Write Excel XLSX files using openpyxl
These functions are not meant to be imported and used. They are examples and templates for common tasks.

Docs: https://openpyxl.readthedocs.io/en/stable/

Tommy Vandermolen
tvandermolen@matrixdesignllc.com
23 July 2021
"""
from __future__ import annotations
from pathlib import Path

import openpyxl


def read_xlsx(path: Path):
    """ Take read-only access of an XLSX file and print some information from it

    :param path: path of the Excel workbook (.xlsx or compatible format, not .xls)
    """
    workbook = openpyxl.load_workbook(path, read_only=True, data_only=True, keep_links=False)

    for sheet in workbook.worksheets:
        print(sheet.title)

    sheet: openpyxl.workbook.workbook.Worksheet = workbook["Sheet1"]
    n = sheet.max_row
    print(f"Sheet has {n} rows")

    for row in sheet.iter_rows(values_only=True):
        print(row)
    workbook.close()


def write_xlsx(path: Path, rows: list[tuple]):
    """ Create or overwrite an .XLSX file and insert a list of rows

    :param path: path of the Excel file to write. Should end in .xlsx or a compatible extension
    :param rows: a list of cells to write to the file
    """
    workbook = openpyxl.Workbook(write_only=True)
    sheet = workbook.create_sheet()
    for row in rows:
        sheet.append(row)
    workbook.save(path)
    workbook.close()


def append_xlsx(path: Path, rows: list[tuple], sheet="Sheet1"):
    """ Append a list of rows to the end of an existing .XLSX spreadsheet

    :param path: path of the Excel file to write. Should end in .xlsx or a compatible extension
    :param rows: a list of cells to write to the file
    :param sheet: name of the sheet to write to (default: Sheet1)
    """
    workbook = openpyxl.load_workbook(path)
    worksheet: openpyxl.workbook.workbook.Worksheet = workbook[sheet]
    for row in rows:
        worksheet.append(row)
    workbook.save(path)
    workbook.close()
