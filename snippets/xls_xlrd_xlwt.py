"""
Read/Write Excel XLS files using xlrd and xlwt
These functions are not meant to be imported and used. They are examples and templates for common tasks.

Docs: https://openpyxl.readthedocs.io/en/stable/

Tommy Vandermolen
tvandermolen@matrixdesignllc.com
26 July 2021
"""
from __future__ import annotations
from pathlib import Path

import xlrd

def read_xls(path: Path):
