"""
Process data with a function in parallel, including a progress bar with estimated time remaining

Docs: https://docs.python.org/3/library/multiprocessing.html#multiprocessing.pool.Pool

Tommy Vandermolen
tvandermolen@matrixdesignllc.com
23 July 2021
"""
import os
import time
from multiprocessing.pool import Pool
# Replace Pool with ThreadPool from here to run multiple threads on the same processor instead of multiple processors
#   (Threads are often just fine for tasks like grabbing files off a network drive, where the network is the delay)
# from multiprocessing.pool import ThreadPool

from helpers.progress_bar import progress_bar
from helpers.str_formatters import str_duration

# Data to process in parallel
data = ["A", "B", "C"]


def fn(input: str):
    """ Function to run in parallel """
    return f"Processed: {input}"


# Run function on data in parallel, returning results in order of completion
with Pool(processes=int(os.cpu_count() * 1.5)) as pool:
    n = len(data)
    start_time = time.monotonic()

    for i, result in enumerate(pool.imap_unordered(fn, data), start=1):
        elapsed_time = time.monotonic() - start_time
        estimated_time_remaining = (n - i) * (elapsed_time / i)
        progress_bar(
            i, n,
            prefix=f"({i:4d}/{n:4d}) ",
            suffix=f" Rem: {str_duration(estimated_time_remaining)}"
        )
