"""
Code snippets for checking and managing resource files

Tommy Vandermolen
tvandermolen@matrixdesignllc.com
30 August 2021
"""

from pathlib import Path
from dataclasses import dataclass
import sys
import logging

logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class Paths:
    """ Resource paths for this script """
    if getattr(sys, "frozen", False):
        # Running from .exe bundle (PyInstaller)
        input = Path("./input/")
        output = Path("./output/")
    else:
        # Running as script
        input = Path("../input/")
        output = Path("../output/")


def check_resource_folders():
    """ Check that the folders defined in `Paths` exist and create any missing ones

    If any critical folder/data is missing, return False, signaling the script to exit

    :return: False if any critical folders/data are missing
    """
    can_continue = True

    if not Paths.output.exists():
        logger.info(f"Making output folder: {Paths.output}")
        Paths.output.mkdir(exist_ok=True)

    if not Paths.input.exists():
        logger.error(f"Input folder does not exist. Creating it: {Paths.input}")
        Paths.input.mkdir(exist_ok=True)
        logger.info("Place spreadsheet in input folder and re-run script")
        can_continue = False

    return can_continue
