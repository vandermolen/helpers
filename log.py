"""
Reusable helper functions for logging to the console and to a log file

Tommy Vandermolen
tvandermolen@matrixdesignllc.com
22 July 2021
"""

import logging
import os
import platform
import sys
from datetime import datetime
from logging.handlers import RotatingFileHandler


def init_logging(logger_name: str, file_name="log.txt", console_level=logging.INFO) -> logging.Logger:
    """ Set up logging to the console and to a log file

    :param logger_name: the name of the logger to use, typically __name__ of the main file
    :param file_name: name of the log file to output
    :param console_level: logging level to print to the console (DEBUG is always sent to the file)
    :return: a Logger instance with the specified settings
    """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)  # Don't change this log level or else debug events won't be written to file

    console_handler = logging.StreamHandler()
    console_handler.setLevel(console_level)
    console_handler.setFormatter(logging.Formatter(r'{levelname:7s}: {message}', style='{'))
    logger.addHandler(console_handler)

    file_handler = RotatingFileHandler(file_name, maxBytes=32000, backupCount=2)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(logging.Formatter(r'{levelname:7s}:{module}:{lineno:<3d}: {message}', style='{'))
    logger.addHandler(file_handler)

    return logger


def log_exception(logger: logging.Logger, ex: Exception, msg=""):
    """ Logs an exception in a clean and user-friendly way

    Only the exception type, exception message, and user message are shown at ERROR level.
    Traceback is printed at DEBUG level, so it can be hidden easily or logged only to a file.

    :param logger: the logger to write to
    :param ex: the exception to log
    :param msg: an optional custom message to show in addition to the exception's message
    """
    if msg:
        logger.error(msg)
    exception_type = type(ex).__name__
    logger.error(f"[{exception_type}] {str(ex)}")
    logger.debug("Traceback", exc_info=True)


def log_system_info(logger: logging.Logger):
    """ Logs system info (OS name, Python version, etc.) to the given logger at the DEBUG level

    :param logger: the logger to write to
    """
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    logger.debug(timestamp)
    logger.debug(os.getcwd())
    logger.debug(f"{platform.node()}")
    logger.debug(f"{platform.platform(aliased=True)}, {platform.machine()}")
    logger.debug(f"{platform.python_implementation()} {platform.python_version()}")
    if getattr(sys, "frozen", False):
        logger.debug("Running as frozen PyInstaller bundle")
